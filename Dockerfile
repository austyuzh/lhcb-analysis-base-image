FROM ubuntu:14.04
MAINTAINER Andrey Ustyuzhanin andrey.ustyuzhanin@cern.ch

# install basic libraries
USER root
RUN apt-get -y update \
    && apt-get -y install build-essential libx11-dev libxpm-dev libxft-dev libxext-dev libpng12-dev libjpeg-dev gfortran libssl-dev libpcre3-dev libgl1-mesa-dev libglew1.5-dev libftgl-dev libmysqlclient-dev libfftw3-dev libcfitsio3-dev graphviz-dev libavahi-compat-libdnssd-dev libldap2-dev libxml2-dev libafterimage0 libafterimage-dev cmake vim emacs git zsh krb5-user krb5-config

# install more stuff such as boost
RUN apt-get -y update \
    && apt-get -y install wget libboost-all-dev

# gsl and clang
RUN apt-get -y update \
    && apt-get -y install libgsl0ldbl libgsl0-dev clang

WORKDIR /tmp


# ROOT with roofit, xrootd and mathmore and minuit2
RUN git clone --depth 1 http://root.cern.ch/git/root.git -b v6-06-00 --single-branch \
    && mkdir root-build \
    && cd root-build \
    && cmake ../root -Dmathmore=ON -Dminuit2=ON -Droofit=ON -Dhdfs=OFF -Dxrootd=ON -Dbuiltin_xrootd=ON -DCMAKE_INSTALL_PREFIX=/usr/local \
    && make -j4 \
    && cmake --build . --target install \
    && cd .. \
    && rm -rf root root-build

RUN apt-get -y update \
    && apt-get -y install libkrb5-dev


RUN git clone --depth 1 http://github.com/xrootd/xrootd.git -b v4.2.3 --single-branch \
    && mkdir xrootd-build \
    && cd xrootd-build \
    && cmake ../xrootd -DCMAKE_INSTALL_PREFIX=/usr/local -DENABLE_PERL=FALSE -DENABLE_FUSE=FALSE -DENABLE_KRB5=TRUE \
    && make && make install \
    && cd .. && rm -rf xrootd xrootd-build

ENV LD_LIBRARY_PATH /usr/local/lib:$LD_LIBRARY_PATH
ENV PYTHONPATH /usr/local/lib:$PYTHONPATH
